import 'package:freezed_annotation/freezed_annotation.dart';

import '../enums/storage_options_enum.dart';

part 'music_model.freezed.dart';

part 'music_model.g.dart';

@freezed
class Music with _$Music {
  const factory Music({
    required String id,
    required String title,
    required String artist,
    required double sizeMb,
    required Duration duration,
    required String songPhotoUrl,
    @Default(StorageStatus.notSaved) StorageStatus storageStatus,
  }) = _Music;

  factory Music.fromJson(Map<String, dynamic> json) => _$MusicFromJson(json);
}