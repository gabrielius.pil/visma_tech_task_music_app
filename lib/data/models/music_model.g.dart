// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'music_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Music _$$_MusicFromJson(Map<String, dynamic> json) => _$_Music(
      id: json['id'] as String,
      title: json['title'] as String,
      artist: json['artist'] as String,
      sizeMb: (json['sizeMb'] as num).toDouble(),
      duration: Duration(microseconds: json['duration'] as int),
      songPhotoUrl: json['songPhotoUrl'] as String,
      storageStatus:
          $enumDecodeNullable(_$StorageStatusEnumMap, json['storageStatus']) ??
              StorageStatus.notSaved,
    );

Map<String, dynamic> _$$_MusicToJson(_$_Music instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'artist': instance.artist,
      'sizeMb': instance.sizeMb,
      'duration': instance.duration.inMicroseconds,
      'songPhotoUrl': instance.songPhotoUrl,
      'storageStatus': _$StorageStatusEnumMap[instance.storageStatus]!,
    };

const _$StorageStatusEnumMap = {
  StorageStatus.memory: 'memory',
  StorageStatus.fileSystem: 'fileSystem',
  StorageStatus.notSaved: 'notSaved',
};
