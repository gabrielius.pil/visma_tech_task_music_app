// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'music_category_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

MusicCategory _$MusicCategoryFromJson(Map<String, dynamic> json) {
  return _MusicCategory.fromJson(json);
}

/// @nodoc
mixin _$MusicCategory {
  String get categoryName => throw _privateConstructorUsedError;
  List<Music> get music => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $MusicCategoryCopyWith<MusicCategory> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MusicCategoryCopyWith<$Res> {
  factory $MusicCategoryCopyWith(
          MusicCategory value, $Res Function(MusicCategory) then) =
      _$MusicCategoryCopyWithImpl<$Res, MusicCategory>;
  @useResult
  $Res call({String categoryName, List<Music> music});
}

/// @nodoc
class _$MusicCategoryCopyWithImpl<$Res, $Val extends MusicCategory>
    implements $MusicCategoryCopyWith<$Res> {
  _$MusicCategoryCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? categoryName = null,
    Object? music = null,
  }) {
    return _then(_value.copyWith(
      categoryName: null == categoryName
          ? _value.categoryName
          : categoryName // ignore: cast_nullable_to_non_nullable
              as String,
      music: null == music
          ? _value.music
          : music // ignore: cast_nullable_to_non_nullable
              as List<Music>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_MusicCategoryCopyWith<$Res>
    implements $MusicCategoryCopyWith<$Res> {
  factory _$$_MusicCategoryCopyWith(
          _$_MusicCategory value, $Res Function(_$_MusicCategory) then) =
      __$$_MusicCategoryCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String categoryName, List<Music> music});
}

/// @nodoc
class __$$_MusicCategoryCopyWithImpl<$Res>
    extends _$MusicCategoryCopyWithImpl<$Res, _$_MusicCategory>
    implements _$$_MusicCategoryCopyWith<$Res> {
  __$$_MusicCategoryCopyWithImpl(
      _$_MusicCategory _value, $Res Function(_$_MusicCategory) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? categoryName = null,
    Object? music = null,
  }) {
    return _then(_$_MusicCategory(
      categoryName: null == categoryName
          ? _value.categoryName
          : categoryName // ignore: cast_nullable_to_non_nullable
              as String,
      music: null == music
          ? _value.music
          : music // ignore: cast_nullable_to_non_nullable
              as List<Music>,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_MusicCategory with DiagnosticableTreeMixin implements _MusicCategory {
  const _$_MusicCategory({required this.categoryName, required this.music});

  factory _$_MusicCategory.fromJson(Map<String, dynamic> json) =>
      _$$_MusicCategoryFromJson(json);

  @override
  final String categoryName;
  @override
  final List<Music> music;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'MusicCategory(categoryName: $categoryName, music: $music)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'MusicCategory'))
      ..add(DiagnosticsProperty('categoryName', categoryName))
      ..add(DiagnosticsProperty('music', music));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_MusicCategory &&
            (identical(other.categoryName, categoryName) ||
                other.categoryName == categoryName) &&
            const DeepCollectionEquality().equals(other.music, music));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType, categoryName, const DeepCollectionEquality().hash(music));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_MusicCategoryCopyWith<_$_MusicCategory> get copyWith =>
      __$$_MusicCategoryCopyWithImpl<_$_MusicCategory>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_MusicCategoryToJson(
      this,
    );
  }
}

abstract class _MusicCategory implements MusicCategory {
  const factory _MusicCategory(
      {required final String categoryName,
      required final List<Music> music}) = _$_MusicCategory;

  factory _MusicCategory.fromJson(Map<String, dynamic> json) =
      _$_MusicCategory.fromJson;

  @override
  String get categoryName;
  @override
  List<Music> get music;
  @override
  @JsonKey(ignore: true)
  _$$_MusicCategoryCopyWith<_$_MusicCategory> get copyWith =>
      throw _privateConstructorUsedError;
}
