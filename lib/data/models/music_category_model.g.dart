// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'music_category_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_MusicCategory _$$_MusicCategoryFromJson(Map<String, dynamic> json) =>
    _$_MusicCategory(
      categoryName: json['categoryName'] as String,
      music: (json['music'] as List<dynamic>)
          .map((e) => Music.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_MusicCategoryToJson(_$_MusicCategory instance) =>
    <String, dynamic>{
      'categoryName': instance.categoryName,
      'music': instance.music.map((e) => e.toJson()).toList(),
    };
