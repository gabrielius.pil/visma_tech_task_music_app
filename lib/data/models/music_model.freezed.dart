// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'music_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Music _$MusicFromJson(Map<String, dynamic> json) {
  return _Music.fromJson(json);
}

/// @nodoc
mixin _$Music {
  String get id => throw _privateConstructorUsedError;
  String get title => throw _privateConstructorUsedError;
  String get artist => throw _privateConstructorUsedError;
  double get sizeMb => throw _privateConstructorUsedError;
  Duration get duration => throw _privateConstructorUsedError;
  String get songPhotoUrl => throw _privateConstructorUsedError;
  StorageStatus get storageStatus => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $MusicCopyWith<Music> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MusicCopyWith<$Res> {
  factory $MusicCopyWith(Music value, $Res Function(Music) then) =
      _$MusicCopyWithImpl<$Res, Music>;
  @useResult
  $Res call(
      {String id,
      String title,
      String artist,
      double sizeMb,
      Duration duration,
      String songPhotoUrl,
      StorageStatus storageStatus});
}

/// @nodoc
class _$MusicCopyWithImpl<$Res, $Val extends Music>
    implements $MusicCopyWith<$Res> {
  _$MusicCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? title = null,
    Object? artist = null,
    Object? sizeMb = null,
    Object? duration = null,
    Object? songPhotoUrl = null,
    Object? storageStatus = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      artist: null == artist
          ? _value.artist
          : artist // ignore: cast_nullable_to_non_nullable
              as String,
      sizeMb: null == sizeMb
          ? _value.sizeMb
          : sizeMb // ignore: cast_nullable_to_non_nullable
              as double,
      duration: null == duration
          ? _value.duration
          : duration // ignore: cast_nullable_to_non_nullable
              as Duration,
      songPhotoUrl: null == songPhotoUrl
          ? _value.songPhotoUrl
          : songPhotoUrl // ignore: cast_nullable_to_non_nullable
              as String,
      storageStatus: null == storageStatus
          ? _value.storageStatus
          : storageStatus // ignore: cast_nullable_to_non_nullable
              as StorageStatus,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_MusicCopyWith<$Res> implements $MusicCopyWith<$Res> {
  factory _$$_MusicCopyWith(_$_Music value, $Res Function(_$_Music) then) =
      __$$_MusicCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String id,
      String title,
      String artist,
      double sizeMb,
      Duration duration,
      String songPhotoUrl,
      StorageStatus storageStatus});
}

/// @nodoc
class __$$_MusicCopyWithImpl<$Res> extends _$MusicCopyWithImpl<$Res, _$_Music>
    implements _$$_MusicCopyWith<$Res> {
  __$$_MusicCopyWithImpl(_$_Music _value, $Res Function(_$_Music) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? title = null,
    Object? artist = null,
    Object? sizeMb = null,
    Object? duration = null,
    Object? songPhotoUrl = null,
    Object? storageStatus = null,
  }) {
    return _then(_$_Music(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      artist: null == artist
          ? _value.artist
          : artist // ignore: cast_nullable_to_non_nullable
              as String,
      sizeMb: null == sizeMb
          ? _value.sizeMb
          : sizeMb // ignore: cast_nullable_to_non_nullable
              as double,
      duration: null == duration
          ? _value.duration
          : duration // ignore: cast_nullable_to_non_nullable
              as Duration,
      songPhotoUrl: null == songPhotoUrl
          ? _value.songPhotoUrl
          : songPhotoUrl // ignore: cast_nullable_to_non_nullable
              as String,
      storageStatus: null == storageStatus
          ? _value.storageStatus
          : storageStatus // ignore: cast_nullable_to_non_nullable
              as StorageStatus,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Music implements _Music {
  const _$_Music(
      {required this.id,
      required this.title,
      required this.artist,
      required this.sizeMb,
      required this.duration,
      required this.songPhotoUrl,
      this.storageStatus = StorageStatus.notSaved});

  factory _$_Music.fromJson(Map<String, dynamic> json) =>
      _$$_MusicFromJson(json);

  @override
  final String id;
  @override
  final String title;
  @override
  final String artist;
  @override
  final double sizeMb;
  @override
  final Duration duration;
  @override
  final String songPhotoUrl;
  @override
  @JsonKey()
  final StorageStatus storageStatus;

  @override
  String toString() {
    return 'Music(id: $id, title: $title, artist: $artist, sizeMb: $sizeMb, duration: $duration, songPhotoUrl: $songPhotoUrl, storageStatus: $storageStatus)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Music &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.title, title) || other.title == title) &&
            (identical(other.artist, artist) || other.artist == artist) &&
            (identical(other.sizeMb, sizeMb) || other.sizeMb == sizeMb) &&
            (identical(other.duration, duration) ||
                other.duration == duration) &&
            (identical(other.songPhotoUrl, songPhotoUrl) ||
                other.songPhotoUrl == songPhotoUrl) &&
            (identical(other.storageStatus, storageStatus) ||
                other.storageStatus == storageStatus));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, id, title, artist, sizeMb,
      duration, songPhotoUrl, storageStatus);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_MusicCopyWith<_$_Music> get copyWith =>
      __$$_MusicCopyWithImpl<_$_Music>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_MusicToJson(
      this,
    );
  }
}

abstract class _Music implements Music {
  const factory _Music(
      {required final String id,
      required final String title,
      required final String artist,
      required final double sizeMb,
      required final Duration duration,
      required final String songPhotoUrl,
      final StorageStatus storageStatus}) = _$_Music;

  factory _Music.fromJson(Map<String, dynamic> json) = _$_Music.fromJson;

  @override
  String get id;
  @override
  String get title;
  @override
  String get artist;
  @override
  double get sizeMb;
  @override
  Duration get duration;
  @override
  String get songPhotoUrl;
  @override
  StorageStatus get storageStatus;
  @override
  @JsonKey(ignore: true)
  _$$_MusicCopyWith<_$_Music> get copyWith =>
      throw _privateConstructorUsedError;
}
