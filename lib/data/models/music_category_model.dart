import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

import 'music_model.dart';

part 'music_category_model.freezed.dart';

part 'music_category_model.g.dart';

@Freezed(makeCollectionsUnmodifiable: false)
class MusicCategory with _$MusicCategory {

  @JsonSerializable(explicitToJson: true)
  const factory MusicCategory({
    required String categoryName,
    required List<Music> music,
  }) = _MusicCategory;

  factory MusicCategory.fromJson(Map<String, dynamic> json) => _$MusicCategoryFromJson(json);
}
