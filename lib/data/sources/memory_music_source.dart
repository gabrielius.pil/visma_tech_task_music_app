
import '../models/music_model.dart';

abstract class MemoryMusicSource {
  List<Music> getSavedMusic();

  void saveInitialMusic(List<Music> music);

  Music saveMusic(Music music);
}

class MemoryMusicSourceImpl extends MemoryMusicSource {
  final List<Music> savedMusic = [];

  @override
  List<Music> getSavedMusic() {
    return savedMusic;
  }

  @override
  void saveInitialMusic(List<Music> music) {
    savedMusic.addAll(music);
  }

  @override
  Music saveMusic(Music music) {
    final index = savedMusic.indexWhere((element) => element.id == music.id);
    savedMusic.removeAt(index);
    savedMusic.insert(index, music);
    return music;
  }
}
