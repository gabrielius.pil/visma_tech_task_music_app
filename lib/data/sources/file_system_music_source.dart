import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

import '../models/music_model.dart';

abstract class FileSystemMusicSource {
  List<Music> getSavedMusic();

  void saveInitialMusic(List<Music> music);

  Future<Music> saveMusic(Music music);
}

class FileSystemMusicSourceImpl extends FileSystemMusicSource {
  static const String _savedMusicKey = 'file_system_music';

  final SharedPreferences sharedPreferences;
  final List<Music> allMusic = [];
  final List<Music> savedMusic = [];

  FileSystemMusicSourceImpl({
    required this.sharedPreferences,
  });

  @override
  List<Music> getSavedMusic() {
    return allMusic;
  }

  @override
  void saveInitialMusic(List<Music> music) {
    savedMusic.addAll(readSavedMusic());
    allMusic.addAll(music);

    if (savedMusic.isEmpty) return;

    for (final m in savedMusic) {
      final index = allMusic.indexWhere((element) => element.id == m.id);
      allMusic.removeAt(index);
      allMusic.insert(index, m);
    }
  }

  List<Music> readSavedMusic() {
    final savedData = sharedPreferences.getStringList(_savedMusicKey);

    return savedData == null
        ? []
        : savedData.map((json) => Music.fromJson(jsonDecode(json))).toList();
  }

  @override
  Future<Music> saveMusic(Music music) async {
    final tempList = savedMusic;
    _replaceMusic(tempList, music);

    await sharedPreferences.setStringList(
      _savedMusicKey,
      tempList.map((m) => jsonEncode(m.toJson())).toList(),
    );

    savedMusic.add(music);
    _replaceMusic(allMusic, music);
    return music;
  }

  void _replaceMusic(List<Music> musicList, Music music) {
    final index = musicList.indexWhere((element) => element.id == music.id);
    if (index != -1) {
      musicList.removeAt(index);
      musicList.insert(index, music);
    } else {
      musicList.add(music);
    }
  }
}
