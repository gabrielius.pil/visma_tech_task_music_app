import '../models/music_category_model.dart';
import '../models/music_model.dart';

abstract class MusicSource {
  List<MusicCategory> getCategorizedMusic();

  List<Music> getAllMusic();
}

class MusicSourceImpl extends MusicSource {
  List<Music> allMusic = [];

  @override
  List<MusicCategory> getCategorizedMusic() {
    return jsonData.map((json) => MusicCategory.fromJson(json)).toList();
  }

  @override
  List<Music> getAllMusic() {
    if (allMusic.isNotEmpty) {
      return allMusic;
    }
    final categorizedMusic = getCategorizedMusic();
    for (var element in categorizedMusic) {
      allMusic.addAll(element.music);
    }
    return allMusic;
  }
}

const jsonData = [
  {
    'categoryName': 'Electronic',
    'music': [
      {
        'id': 'a310cc04-43fc-11ee-be56-0242ac120002',
        'title': 'Places',
        'artist': 'The Blaze',
        'sizeMb': 12.34,
        'duration': 184200000,
        'songPhotoUrl':
            'https://images.genius.com/ed345046c62880a77f43552cc2dc8e90.1000x1000x1.png'
      },
      {
        'id': 'a310cf10-43fc-11ee-be56-0242ac120002',
        'title': 'Never Ending',
        'artist': 'Bob Moses',
        'sizeMb': 9.31,
        'duration': 172200000,
        'songPhotoUrl':
            'https://i1.sndcdn.com/artworks-eldQRVnFZycQ-0-t500x500.jpg'
      },
      {
        'id': 'a310d0b4-43fc-11ee-be56-0242ac120002',
        'title': 'Where You Are',
        'artist': 'John Summit, Hayla',
        'sizeMb': 22.34,
        'duration': 214200000,
        'songPhotoUrl':
            'https://i1.sndcdn.com/artworks-sGCXuDeG4CZ4-0-t500x500.jpg'
      },
      {
        'id': 'a310d226-43fc-11ee-be56-0242ac120002',
        'title':
            'Catching Eyes - with some realy long description, that should be checked',
        'artist': '49th & Main',
        'sizeMb': 15.34,
        'duration': 123300000,
        'songPhotoUrl':
            'https://i1.sndcdn.com/artworks-Gev8VFqmDiD4-0-t500x500.png'
      },
      {
        'id': 'a310d74e-43fc-11ee-be56-0242ac120002',
        'title': 'Water',
        'artist': 'BAYNK',
        'sizeMb': 9.31,
        'duration': 172200000,
        'songPhotoUrl':
            'https://i0.wp.com/aonetwothreefour.co/wp-content/uploads/2019/04/42987677_1943755922381453_7443504745777463296_o.jpg?fit=1000%2C1000&ssl=1&w=640'
      },
      {
        'id': 'a310d8de-43fc-11ee-be56-0242ac120002',
        'title': 'White Noise',
        'artist': 'Disclosure, AlunaGeorge',
        'sizeMb': 22.34,
        'duration': 214200000,
        'songPhotoUrl':
            'https://images.genius.com/1f45dc52c65bae50b9263770eaa97b89.1000x1000x1.jpg'
      },
      {
        'id': 'a310da32-43fc-11ee-be56-0242ac120002',
        'title': 'Let Go',
        'artist': 'Duskus',
        'sizeMb': 15.34,
        'duration': 123300000,
        'songPhotoUrl':
            'https://i1.sndcdn.com/artworks-FkJWs3mbc8Sa-0-t500x500.png'
      },
    ],
  },
  {
    'categoryName': 'Chill Tracks',
    'music': [
      {
        'id': 'a310db5e-43fc-11ee-be56-0242ac120002',
        'title': 'Just Over',
        'artist': 'Yotto',
        'sizeMb': 12.34,
        'duration': 184200000,
        'songPhotoUrl':
            'https://i1.sndcdn.com/artworks-100d7f60-b921-4151-abe2-7e9760c6464a-0-t500x500.jpg'
      },
      {
        'id': 'a310dc8a-43fc-11ee-be56-0242ac120002',
        'title': 'Salta',
        'artist': 'Sultan + Shepard',
        'sizeMb': 9.31,
        'duration': 172200000,
        'songPhotoUrl': 'https://f4.bcbits.com/img/a2216565293_65'
      },
      {
        'id': 'a310ddb6-43fc-11ee-be56-0242ac120002',
        'title': 'Howl',
        'artist': 'Elderbrook, Tourist',
        'sizeMb': 22.34,
        'duration': 214200000,
        'songPhotoUrl':
            'https://i1.sndcdn.com/artworks-eZ0pWjFAy8Jz-0-t500x500.jpg'
      },
      {
        'id': 'a310dee2-43fc-11ee-be56-0242ac120002',
        'title': 'I Need You',
        'artist': 'Elderbrook',
        'sizeMb': 15.34,
        'duration': 123300000,
        'songPhotoUrl':
            'https://images.genius.com/d060753b6a633c15dbb1146b89b08bd0.999x999x1.png'
      },
      {
        'id': 'a310e342-43fc-11ee-be56-0242ac120002',
        'title': 'The Chase',
        'artist': 'Emmit Fenn',
        'sizeMb': 9.31,
        'duration': 172200000,
        'songPhotoUrl':
            'https://i1.sndcdn.com/artworks-AIlHwOxJ9c3w-0-t500x500.png'
      },
      {
        'id': 'a310e50e-43fc-11ee-be56-0242ac120002',
        'title': 'The Best Part',
        'artist': 'aname, gardenstate, Bien',
        'sizeMb': 22.34,
        'duration': 214200000,
        'songPhotoUrl':
            'https://i1.sndcdn.com/artworks-5tFE02Kd0hZLy4mh-P13iew-t500x500.jpg'
      },
      {
        'id': 'a310e658-43fc-11ee-be56-0242ac120002',
        'title': 'Rule The World',
        'artist': 'ricky retro, Scorz',
        'sizeMb': 15.34,
        'duration': 123300000,
        'songPhotoUrl':
            'https://i1.sndcdn.com/artworks-xmdxCFZhXlL5-0-t500x500.png'
      },
      {
        'id': 'a310e7fc-43fc-11ee-be56-0242ac120002',
        'title': 'Gravity',
        'artist': 'Lastlings',
        'sizeMb': 18.34,
        'duration': 33300000,
        'songPhotoUrl':
            'https://i1.sndcdn.com/artworks-VTOfvQCC9T4M-0-t500x500.jpg'
      },
    ],
  },
];
