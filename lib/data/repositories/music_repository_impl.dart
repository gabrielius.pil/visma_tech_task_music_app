import '../../domain/repositories/music_repository.dart';
import '../models/music_category_model.dart';
import '../sources/music_source.dart';

class MusicRepositoryImpl implements MusicRepository {

  final MusicSource musicSource;

  MusicRepositoryImpl({required this.musicSource});

  @override
  List<MusicCategory> getCategorizedMusic(){
    return musicSource.getCategorizedMusic();
  }
}
