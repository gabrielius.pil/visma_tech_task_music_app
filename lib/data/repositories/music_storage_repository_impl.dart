import '../../domain/repositories/music_storage_repository.dart';
import '../models/music_model.dart';
import '../sources/file_system_music_source.dart';
import '../sources/memory_music_source.dart';
import '../sources/music_source.dart';

class MusicStorageRepositoryImpl extends MusicStorageRepository {
  final MemoryMusicSource memoryMusicSource;
  final FileSystemMusicSource fileSystemMusicSource;
  final MusicSource musicSource;

  MusicStorageRepositoryImpl({
    required this.memoryMusicSource,
    required this.fileSystemMusicSource,
    required this.musicSource,
  });

  @override
  List<Music> getInitialMusicList() {
    return musicSource.getAllMusic();
  }

  @override
  List<Music> getMemorySavedMusic() {
    return memoryMusicSource.getSavedMusic();
  }

  @override
  void saveInitialMusicToMemory(List<Music> music) {
    memoryMusicSource.saveInitialMusic(music);
  }

  @override
  void saveInitialMusicToFileSystemMemory(List<Music> music) {
    fileSystemMusicSource.saveInitialMusic(music);
  }

  @override
  List<Music> getFileSystemSavedMusic() {
    return fileSystemMusicSource.getSavedMusic();
  }

  @override
  Music saveMusicToMemory(Music music) {
    return memoryMusicSource.saveMusic(music);
  }

  @override
  Future<Music> saveMusicToFileSystem(Music music) {
    return fileSystemMusicSource.saveMusic(music);
  }
}
