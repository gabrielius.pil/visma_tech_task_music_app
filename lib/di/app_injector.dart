import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../data/repositories/music_repository_impl.dart';
import '../../data/sources/music_source.dart';
import '../../domain/repositories/music_repository.dart';
import '../../domain/use_cases/get_music_use_case.dart';
import '../data/repositories/music_storage_repository_impl.dart';
import '../data/sources/file_system_music_source.dart';
import '../data/sources/memory_music_source.dart';
import '../domain/repositories/music_storage_repository.dart';
import '../domain/use_cases/music_storage_use_case.dart';

class AppInjector {
  static GetIt getIt = GetIt.instance;

  static Future<void> setupInjector() async {
    await _setUpExternals();
    _setUpSources();
    _setUpRepositories();
    _setupUseCases();
  }

  static Future<void> _setUpExternals() async {
    final sharedPreferences = await SharedPreferences.getInstance();

    getIt.registerLazySingleton<SharedPreferences>(
      () => sharedPreferences,
    );
  }

  static _setUpSources() {
    getIt.registerLazySingleton<MusicSource>(
      () => MusicSourceImpl(),
    );
    getIt.registerLazySingleton<MemoryMusicSource>(
      () => MemoryMusicSourceImpl(),
    );
    getIt.registerLazySingleton<FileSystemMusicSource>(
      () => FileSystemMusicSourceImpl(
        sharedPreferences: getIt.get<SharedPreferences>(),
      ),
    );
  }

  static _setUpRepositories() {
    getIt.registerLazySingleton<MusicRepository>(
      () => MusicRepositoryImpl(musicSource: getIt.get<MusicSource>()),
    );

    getIt.registerLazySingleton<MusicStorageRepository>(
      () => MusicStorageRepositoryImpl(
        memoryMusicSource: getIt.get<MemoryMusicSource>(),
        fileSystemMusicSource: getIt.get<FileSystemMusicSource>(),
        musicSource: getIt.get<MusicSource>(),
      ),
    );
  }

  static _setupUseCases() {
    getIt.registerLazySingleton<GetMusicUseCase>(
      () => GetMusicUseCase(musicRepository: getIt.get<MusicRepository>()),
    );
    getIt.registerLazySingleton<MusicStorageUseCase>(
      () => MusicStorageUseCase(
        musicStorageRepository: getIt.get<MusicStorageRepository>(),
      ),
    );
  }
}
