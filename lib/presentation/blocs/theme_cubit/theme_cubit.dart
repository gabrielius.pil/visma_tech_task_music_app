import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'theme_state.dart';

class ThemeCubit extends Cubit<ThemeState> {
  ThemeCubit(ThemeMode mode,) : super(ThemeState(mode));

  void changeTheme(ThemeMode mode) {
    emit(ThemeState(mode));
  }

  void toggle() {
    final ThemeMode mode =
        state.mode == ThemeMode.light ? ThemeMode.dark : ThemeMode.light;
    emit(ThemeState(mode));
  }
}
