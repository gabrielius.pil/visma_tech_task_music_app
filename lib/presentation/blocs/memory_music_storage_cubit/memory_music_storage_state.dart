part of 'memory_music_storage_cubit.dart';

class MemoryMusicStorageState extends Equatable {
  final List<Music> music;
  final Duration savedMusicsDuration;

  const MemoryMusicStorageState({required this.music ,required this.savedMusicsDuration});

  @override
  List<Object?> get props => [music, savedMusicsDuration];

}

