import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../data/models/music_model.dart';
import '../../../domain/use_cases/music_storage_use_case.dart';

part 'memory_music_storage_state.dart';

class MemoryMusicStorageCubit extends Cubit<MemoryMusicStorageState> {
  final MusicStorageUseCase musicStorageUseCase;

  MemoryMusicStorageCubit({
    required this.musicStorageUseCase,
  }) : super(const MemoryMusicStorageState(
      music: [], savedMusicsDuration: Duration.zero));

  void initMusic() {
    emit(MemoryMusicStorageState(
      music: musicStorageUseCase.getInitialMemoryMusicList(),
      savedMusicsDuration: state.savedMusicsDuration,
    ));
  }

  void saveMusicToMemory(Music music) {
    final savedMusic = musicStorageUseCase.saveMusicToMemory(music);
    emit(MemoryMusicStorageState(
      music: musicStorageUseCase.getMemorySavedMusic(),
      savedMusicsDuration: state.savedMusicsDuration + savedMusic.duration,
    ));
  }
}
