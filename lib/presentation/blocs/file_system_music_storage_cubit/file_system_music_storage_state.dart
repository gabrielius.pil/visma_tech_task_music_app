part of 'file_system_music_storage_cubit.dart';

abstract class FileSystemMusicStorageState extends Equatable {
  final List<Music> music;
  final Duration savedMusicsDuration;

  const FileSystemMusicStorageState({
    required this.music,
    required this.savedMusicsDuration,
  });

  @override
  List<Object> get props => [music, savedMusicsDuration];
}

class FileSystemMusicStorageSavedMusic extends FileSystemMusicStorageState {
  const FileSystemMusicStorageSavedMusic({
    required super.music,
    required super.savedMusicsDuration,
  });
}

class FileSystemMusicStorageSavingInProgress
    extends FileSystemMusicStorageState {
  final Music musicBeingSaved;

  const FileSystemMusicStorageSavingInProgress({
    required this.musicBeingSaved,
    required super.music,
    required super.savedMusicsDuration,
  });

  @override
  List<Object> get props => [musicBeingSaved, music, savedMusicsDuration];
}

class FileSystemMusicStorageFailure extends FileSystemMusicStorageState {
  final String message;

  const FileSystemMusicStorageFailure({
    required this.message,
    required super.music,
    required super.savedMusicsDuration,
  });

  @override
  List<Object> get props => [message, music, savedMusicsDuration];
}
