import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

import '../../../data/enums/storage_options_enum.dart';
import '../../../data/models/music_model.dart';
import '../../../domain/use_cases/music_storage_use_case.dart';

part 'file_system_music_storage_state.dart';

class FileSystemMusicStorageCubit extends Cubit<FileSystemMusicStorageState> {
  final MusicStorageUseCase musicStorageUseCase;

  FileSystemMusicStorageCubit({
    required this.musicStorageUseCase,
  }) : super(const FileSystemMusicStorageSavedMusic(
          music: [],
          savedMusicsDuration: Duration.zero,
        ));

  void initMusic() {
    final initialMusicList =
        musicStorageUseCase.getInitialFileSystemMusicList();
    final savedMusicsDuration = _getInitialMusicsDuration(initialMusicList);
    emit(FileSystemMusicStorageSavedMusic(
      music: initialMusicList,
      savedMusicsDuration: savedMusicsDuration,
    ));
  }

  Future<void> saveMusicToFileSystem(Music music) async {
    try {
      emit(FileSystemMusicStorageSavingInProgress(
        musicBeingSaved: music,
        music: state.music,
        savedMusicsDuration: state.savedMusicsDuration,
      ));
      final savedMusic = await musicStorageUseCase.saveMusicToFileSystem(music);
      emit(FileSystemMusicStorageSavedMusic(
        music: musicStorageUseCase.getFileSystemSavedMusic(),
        savedMusicsDuration: state.savedMusicsDuration + savedMusic.duration,
      ));
    } catch (error) {
      debugPrint('Error while writing to Shared preferences: $error');
      emit(
        FileSystemMusicStorageFailure(
          message: 'Unexpected error. Please try again later',
          music: state.music,
          savedMusicsDuration: state.savedMusicsDuration,
        ),
      );
    }
  }

  Duration _getInitialMusicsDuration(List<Music> music) {
    var duration = Duration.zero;

    music
        .where((element) => element.storageStatus == StorageStatus.fileSystem)
        .forEach((element) {
      duration = duration + element.duration;
    });
    return duration;
  }
}
