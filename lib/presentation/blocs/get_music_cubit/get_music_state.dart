part of 'get_music_cubit.dart';

abstract class GetMusicState extends Equatable {
  const GetMusicState();

  @override
  List<Object> get props => [];
}

class GetMusicInitial extends GetMusicState {}

class GetMusicLoaded extends GetMusicState {
  final List<MusicCategory> musicCategories;

  const GetMusicLoaded({required this.musicCategories});

  @override
  List<Object> get props => [musicCategories];
}

class GetMusicError extends GetMusicState {
  final String message;

  const GetMusicError({required this.message});

  @override
  List<Object> get props => [message];
}
