import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../data/models/music_category_model.dart';
import '../../../domain/use_cases/get_music_use_case.dart';

part 'get_music_state.dart';

class GetMusicCubit extends Cubit<GetMusicState> {
  final GetMusicUseCase getMusicUseCase;

  GetMusicCubit({required this.getMusicUseCase}) : super(GetMusicInitial());

  void getCategorizedMusic() {
    try {
      var musicCategories = getMusicUseCase.getCategorizedMusic();
      emit(GetMusicLoaded(musicCategories: musicCategories));
    } catch (err) {
      emit(GetMusicError(message: err.toString()));
    }
  }
}
