import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../data/enums/storage_options_enum.dart';
import '../../blocs/file_system_music_storage_cubit/file_system_music_storage_cubit.dart';
import '../widgets/music_list_item.dart';

class FileSystemSavedMusicList extends StatelessWidget {
  const FileSystemSavedMusicList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            pinned: true,
            expandedHeight: MediaQuery.of(context).size.height * 0.15,
            flexibleSpace: const FlexibleSpaceBar(
              centerTitle: false,
              titlePadding: EdgeInsets.only(left: 50, bottom: 12),
              title: Text(
                'File System',
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          ),
          BlocBuilder<FileSystemMusicStorageCubit, FileSystemMusicStorageState>(
            builder: (context, state) {
              return SliverPadding(
                padding: const EdgeInsets.only(top: 20),
                sliver: SliverList(
                  delegate: SliverChildBuilderDelegate(
                    (context, index) => MusicListItem(
                      music: state.music[index],
                      showSavingOption: true,
                      storageOption: StorageStatus.fileSystem,
                    ),
                    childCount: state.music.length,
                  ),
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
