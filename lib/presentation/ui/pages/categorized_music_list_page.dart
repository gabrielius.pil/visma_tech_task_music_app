import 'package:flutter/material.dart';

import '../../../data/models/music_category_model.dart';
import '../widgets/music_list_item.dart';

class CategorizedMusicList extends StatelessWidget {
  final MusicCategory musicCategory;

  const CategorizedMusicList({Key? key, required this.musicCategory})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            pinned: true,
            expandedHeight: MediaQuery.of(context).size.height * 0.15,
            flexibleSpace: FlexibleSpaceBar(
              centerTitle: false,
              titlePadding: const EdgeInsets.only(left: 50, bottom: 12),
              title: Text(
                musicCategory.categoryName,
                style: const TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          ),
          SliverPadding(
            padding: const EdgeInsets.only(top: 20),
            sliver: SliverList(
              delegate: SliverChildBuilderDelegate(
                (context, index) => MusicListItem(
                  music: musicCategory.music[index],
                ),
                childCount: musicCategory.music.length,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
