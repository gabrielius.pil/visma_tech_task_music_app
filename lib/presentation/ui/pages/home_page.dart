import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../data/models/music_category_model.dart';
import '../../blocs/get_music_cubit/get_music_cubit.dart';
import '../widgets/file_system_storage_row.dart';
import '../widgets/memory_storage_row.dart';
import '../widgets/music_category_card.dart';
import '../widgets/theme_switcher_icon.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<GetMusicCubit>(context).getCategorizedMusic();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            pinned: true,
            expandedHeight: MediaQuery.of(context).size.height * 0.15,
            actions: const [
              ThemeSwitcherIcon(),
            ],
            flexibleSpace: const FlexibleSpaceBar(
              centerTitle: false,
              titlePadding: EdgeInsets.only(left: 24, bottom: 12),
              title: Text(
                'Hey, Visma!',
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          ),
          BlocBuilder<GetMusicCubit, GetMusicState>(
            builder: (context, state) {
              if (state is GetMusicLoaded) {
                return _buildCategoriesList(state.musicCategories);
              } else if (state is GetMusicError) {
                return _buildErrorWidget(state.message);
              } else {
                return _buildLoader();
              }
            },
          ),
          const SliverToBoxAdapter(
            child: Padding(
              padding: EdgeInsets.only(top:24.0, right: 24, left: 24, bottom: 12),
              child: Text(
                'Storage',
                style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
              ),
            ),
          ),
          const SliverToBoxAdapter(
            child: MemoryStorageRow(),
          ),
          const SliverToBoxAdapter(
            child: FileSystemStorageRow(),
          ),
          const SliverToBoxAdapter(
            child: SizedBox(
              height: 40,
            ),
          )
        ],
      ),
    );
  }

  Widget _buildCategoriesList(List<MusicCategory> musicCategories) {
    return SliverList(
      delegate: SliverChildBuilderDelegate(
        (context, index) => MusicCategoryCard(
          musicCategory: musicCategories[index],
        ),
        childCount: musicCategories.length,
      ),
    );
  }

  Widget _buildErrorWidget(String message) {
    return SliverToBoxAdapter(
      child: Center(
        child: Text(message),
      ),
    );
  }

  Widget _buildLoader() {
    return const SliverToBoxAdapter(
      child: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}
