import 'package:flutter/material.dart';

class AppColors {
  AppColors._();
  static const primaryLightColor = Color(0xffF7F7F7);
  static const secondaryLightColor = Color(0xffFFFFFF);
  static const accentColor = Color(0xffE44861);

  static const primaryDarkColor = Color(0xff131322);
  static const secondaryDarkColor = Color(0xff191C2F);

  static const white = Color(0xffF9F9FC);

  static const shadowColor = Color(0x27222536);
  static const darkShadowColor = Color(0x33000000);

  static const errorColor = Color(0xffD0320F);
  static const errorLightColor = Color(0xffD35D5D);
  static const errorDarkColor = Color(0xffFF3737);
}
