import 'package:flutter/material.dart';

import 'app_colors.dart';

class AppThemes {
  const AppThemes._();

  static final ThemeData lightTheme = ThemeData(
    useMaterial3: true,
    colorScheme: _getColorScheme(),
    appBarTheme: _getAppBarTheme(),
    scaffoldBackgroundColor: AppColors.primaryLightColor,
    textButtonTheme: _getTextButtonTheme(),
    iconButtonTheme: _getIconButtonTheme(),
    cardTheme: _getCardTheme(),
    dialogTheme: _getDialogTheme(),
    splashColor: AppColors.white.withAlpha(128),
    splashFactory: InkRipple.splashFactory,
    dividerTheme: DividerThemeData(
      color: AppColors.primaryDarkColor.withOpacity(0.1),
    ),
  );

  static final ThemeData darkTheme = ThemeData(
    useMaterial3: true,
    colorScheme: _getDarkColorScheme(),
    appBarTheme: _getAppBarTheme(),
    scaffoldBackgroundColor: AppColors.primaryDarkColor,
    textButtonTheme: _getTextDarkButtonTheme(),
    cardTheme: _getDarkCardTheme(),
    dialogTheme: _getDarkDialogTheme(),
    splashColor: AppColors.white.withAlpha(128),
    splashFactory: InkRipple.splashFactory,
    dividerTheme: DividerThemeData(
      color: AppColors.primaryLightColor.withOpacity(0.2),
    ),
  );

  // Whole app color schemes, that defines all possible scenarios for colors
  static ColorScheme _getColorScheme() {
    return const ColorScheme(
      brightness: Brightness.light,
      primary: AppColors.primaryLightColor,
      onPrimary: AppColors.white,
      primaryContainer: AppColors.secondaryDarkColor,
      onPrimaryContainer: AppColors.white,
      secondary: AppColors.accentColor,
      onSecondary: AppColors.white,
      background: AppColors.primaryLightColor,
      onBackground: AppColors.primaryDarkColor,
      surface: AppColors.secondaryLightColor,
      onSurface: AppColors.secondaryDarkColor,
      error: AppColors.errorLightColor,
      onError: Colors.white,
    );
  }

  static ColorScheme _getDarkColorScheme() {
    return const ColorScheme(
      brightness: Brightness.dark,
      primary: AppColors.primaryDarkColor,
      onPrimary: AppColors.white,
      primaryContainer: AppColors.secondaryDarkColor,
      onPrimaryContainer: AppColors.white,
      secondary: AppColors.accentColor,
      onSecondary: AppColors.white,
      background: AppColors.primaryDarkColor,
      onBackground: AppColors.primaryLightColor,
      surface: AppColors.secondaryDarkColor,
      onSurface: AppColors.secondaryLightColor,
      error: AppColors.errorDarkColor,
      onError: AppColors.white,
    );
  }

  // Text Button themes
  static TextButtonThemeData _getTextButtonTheme() {
    return TextButtonThemeData(
      style: ButtonStyle(
        textStyle: MaterialStateProperty.all(
          const TextStyle(
            fontWeight: FontWeight.w600,
            fontSize: 16,
          ),
        ),
        foregroundColor: MaterialStateProperty.resolveWith<Color>(
          (Set<MaterialState> states) {
            return AppColors.accentColor;
          },
        ),
      ),
    );
  }

  static TextButtonThemeData _getTextDarkButtonTheme() {
    return TextButtonThemeData(
      style: ButtonStyle(
        textStyle: MaterialStateProperty.all(
          const TextStyle(
            fontWeight: FontWeight.w600,
            fontSize: 16,
          ),
        ),
        foregroundColor: MaterialStateProperty.resolveWith<Color>(
          (Set<MaterialState> states) {
            return AppColors.accentColor;
          },
        ),
      ),
    );
  }

  static AppBarTheme _getAppBarTheme() {
    return const AppBarTheme(
      shadowColor: AppColors.shadowColor,
      backgroundColor: AppColors.primaryDarkColor,
      foregroundColor: AppColors.white,
      centerTitle: false,
      iconTheme: IconThemeData(color: AppColors.accentColor),
      titleTextStyle: TextStyle(
        fontSize: 24,
        fontWeight: FontWeight.bold,
      ),
    );
  }

  static CardTheme _getCardTheme() {
    return CardTheme(
      elevation: 6,
      color: AppColors.secondaryLightColor,
      surfaceTintColor: AppColors.secondaryLightColor,
      shadowColor: AppColors.shadowColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5),
      ),
    );
  }

  static CardTheme _getDarkCardTheme() {
    return CardTheme(
      elevation: 6,
      color: AppColors.secondaryDarkColor,
      surfaceTintColor: AppColors.secondaryDarkColor,
      shadowColor: AppColors.darkShadowColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5),
      ),
    );
  }

  static DialogTheme _getDialogTheme() {
    return const DialogTheme(
      backgroundColor: AppColors.secondaryLightColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(5),
        ),
      ),
    );
  }

  static DialogTheme _getDarkDialogTheme() {
    return const DialogTheme(
      backgroundColor: AppColors.secondaryDarkColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(5),
        ),
      ),
    );
  }

  static IconButtonThemeData _getIconButtonTheme() {
    return IconButtonThemeData(style: ButtonStyle(
      foregroundColor: MaterialStateProperty.resolveWith<Color>(
        (Set<MaterialState> states) {
          return AppColors.accentColor;
        },
      ),
    ));
  }
}
