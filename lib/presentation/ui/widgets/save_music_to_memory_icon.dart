import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../data/enums/storage_options_enum.dart';
import '../../../data/models/music_model.dart';
import '../../blocs/memory_music_storage_cubit/memory_music_storage_cubit.dart';

class SaveMusicToMemoryIcon extends StatelessWidget {
  final Music music;

  const SaveMusicToMemoryIcon({
    Key? key,
    required this.music,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: music.storageStatus == StorageStatus.notSaved
          ? () => BlocProvider.of<MemoryMusicStorageCubit>(context)
              .saveMusicToMemory(music)
          : null,
      icon: Icon(
        music.storageStatus == StorageStatus.notSaved
            ? Icons.save
            : Icons.check,
      ),
    );
  }
}
