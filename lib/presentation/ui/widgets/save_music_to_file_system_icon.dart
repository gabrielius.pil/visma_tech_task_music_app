import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../data/enums/storage_options_enum.dart';
import '../../../data/models/music_model.dart';
import '../../blocs/file_system_music_storage_cubit/file_system_music_storage_cubit.dart';
import '../themes/app_colors.dart';

class SaveMusicToFileSystemIcon extends StatelessWidget {
  final Music music;

  const SaveMusicToFileSystemIcon({
    Key? key,
    required this.music,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: music.storageStatus == StorageStatus.notSaved
          ? () => BlocProvider.of<FileSystemMusicStorageCubit>(context)
              .saveMusicToFileSystem(music)
          : null,
      icon: BlocConsumer<FileSystemMusicStorageCubit,
          FileSystemMusicStorageState>(
        listener: (context, state) {
          if (state is FileSystemMusicStorageFailure) {
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text(state.message),
              backgroundColor: Colors.red,
            ));
          }
        },
        builder: (context, state) {
          if (state is FileSystemMusicStorageSavingInProgress &&
              state.musicBeingSaved == music) {
            return const SizedBox.square(
              dimension: 16,
              child: CircularProgressIndicator(
                strokeWidth: 2,
                color: AppColors.accentColor,
              ),
            );
          }
          return Icon(
            music.storageStatus == StorageStatus.notSaved
                ? Icons.save
                : Icons.check,
          );
        },
      ),
    );
  }
}
