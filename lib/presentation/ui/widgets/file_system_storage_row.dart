import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:visma_tech_task_gabrielius_pileckis/extensions/duration_extension.dart';

import '../../blocs/file_system_music_storage_cubit/file_system_music_storage_cubit.dart';
import '../pages/file_system_saved_music_list_page.dart';
import '../themes/app_colors.dart';

class FileSystemStorageRow extends StatelessWidget {
  const FileSystemStorageRow({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        children: [
          InkWell(
            onTap: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (_) => const FileSystemSavedMusicList(),
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 4.0),
              child: Row(
                children: [
                  const Text(
                    'File System',
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 17,
                    ),
                  ),
                  const Spacer(),
                  BlocBuilder<FileSystemMusicStorageCubit,
                      FileSystemMusicStorageState>(
                    builder: (context, state) {
                      if (state is FileSystemMusicStorageSavedMusic) {
                        return Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: Text(
                            state.savedMusicsDuration.formatDurationToSongsLength(),
                            style: const TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 15,
                            ),
                          ),
                        );
                      } else {
                        return const CircularProgressIndicator();
                      }
                    },
                  ),
                  const Icon(
                    Icons.chevron_right,
                    color: AppColors.accentColor,
                  ),
                ],
              ),
            ),
          ),
          const Divider(),
        ],
      ),
    );
  }
}
