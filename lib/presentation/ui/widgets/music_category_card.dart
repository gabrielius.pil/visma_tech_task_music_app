import 'package:flutter/material.dart';

import '../../../data/models/music_category_model.dart';
import '../pages/categorized_music_list_page.dart';
import 'music_card.dart';

class MusicCategoryCard extends StatelessWidget {
  final MusicCategory musicCategory;

  const MusicCategoryCard({Key? key, required this.musicCategory})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(right: 24, left: 24, top: 12),
          child: Row(
            children: [
              Text(
                musicCategory.categoryName,
                style: const TextStyle(
                  fontSize: 22,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const Spacer(),
              TextButton(
                onPressed: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (_) =>
                        CategorizedMusicList(musicCategory: musicCategory),
                  ),
                ),
                child: const Text('See all'),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 265,
          child: ListView.builder(
            padding: const EdgeInsets.only(left: 8, right: 8, bottom: 8),
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) => MusicCard(
              music: musicCategory.music[index],
            ),
            itemCount: musicCategory.music.length >= 5
                ? 5
                : musicCategory.music.length,
          ),
        ),
      ],
    );
  }
}
