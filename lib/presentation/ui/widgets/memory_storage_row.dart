import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:visma_tech_task_gabrielius_pileckis/extensions/duration_extension.dart';

import '../../blocs/memory_music_storage_cubit/memory_music_storage_cubit.dart';
import '../pages/memory_saved_music_list_page.dart';
import '../themes/app_colors.dart';

class MemoryStorageRow extends StatelessWidget {
  const MemoryStorageRow({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        children: [
          InkWell(
            onTap: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (_) => const MemorySavedMusicList(),
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 4.0),
              child: Row(
                children: [
                  const Text(
                    'Memory',
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 17,
                    ),
                  ),
                  const Spacer(),
                  BlocBuilder<MemoryMusicStorageCubit, MemoryMusicStorageState>(
                    builder: (context, state) {
                      return Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: Text(
                          state.savedMusicsDuration.formatDurationToSongsLength(),
                          style: const TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 15,
                          ),
                        ),
                      );
                    },
                  ),
                  const Icon(
                    Icons.chevron_right,
                    color: AppColors.accentColor,
                  ),
                ],
              ),
            ),
          ),
          const Divider(),
        ],
      ),
    );
  }
}
