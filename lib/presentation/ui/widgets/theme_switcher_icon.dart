import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../blocs/theme_cubit/theme_cubit.dart';

class ThemeSwitcherIcon extends StatelessWidget {
  const ThemeSwitcherIcon({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () {
        BlocProvider.of<ThemeCubit>(context).toggle();
      },
      icon: BlocBuilder<ThemeCubit, ThemeState>(
        builder: (context, state) {
          return Icon(
            state.mode == ThemeMode.dark ? Icons.light_mode : Icons.dark_mode,
          );
        },
      ),
    );
  }
}
