import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:visma_tech_task_gabrielius_pileckis/extensions/duration_extension.dart';

import '../../../data/enums/storage_options_enum.dart';
import '../../../data/models/music_model.dart';
import '../themes/app_colors.dart';
import 'save_music_to_file_system_icon.dart';
import 'save_music_to_memory_icon.dart';

class MusicListItem extends StatelessWidget {
  final Music music;
  final bool showSavingOption;
  final StorageStatus storageOption;

  const MusicListItem({
    Key? key,
    required this.music,
    this.showSavingOption = false,
    this.storageOption = StorageStatus.notSaved,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 12.0),
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  height: 60,
                  width: 60,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    image: DecorationImage(
                      image: CachedNetworkImageProvider(
                        music.songPhotoUrl,
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 4.0, left: 8, right: 8),
                                child: Text(
                                  music.title,
                                  style: const TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w600,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                  maxLines: 2,
                                ),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 8.0),
                                child: Text(
                                  music.artist,
                                  style: const TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    color: AppColors.accentColor,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                  maxLines: 1,
                                ),
                              ),
                            ],
                          ),
                        ),
                        if (showSavingOption &&
                            storageOption != StorageStatus.notSaved)
                          storageOption == StorageStatus.memory
                              ? SaveMusicToMemoryIcon(
                                  music: music,
                                )
                              : SaveMusicToFileSystemIcon(
                                  music: music,
                                ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 4,
                        right: 8,
                        left: 8,
                      ),
                      child: Row(
                        children: [
                          Text(
                            '${music.sizeMb} MB',
                            style: const TextStyle(
                              fontSize: 13,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                          const Spacer(),
                          Text(
                            music.duration.formatDurationToSongsLength(),
                            style: const TextStyle(
                              fontSize: 13,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
          const Padding(
            padding: EdgeInsets.symmetric(vertical: 4.0),
            child: Divider(),
          ),
        ],
      ),
    );
  }
}
