import '../../data/models/music_model.dart';

abstract class MusicStorageRepository {
  List<Music> getInitialMusicList();

  void saveInitialMusicToMemory(List<Music> music);

  void saveInitialMusicToFileSystemMemory(List<Music> music);

  List<Music> getMemorySavedMusic();

  List<Music> getFileSystemSavedMusic();

  Music saveMusicToMemory(Music music);

  Future<Music> saveMusicToFileSystem(Music music);
}
