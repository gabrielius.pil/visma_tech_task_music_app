import '../../data/models/music_category_model.dart';

abstract class MusicRepository {
  List<MusicCategory> getCategorizedMusic();
}
