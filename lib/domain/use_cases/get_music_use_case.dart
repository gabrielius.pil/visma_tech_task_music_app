import '../../data/models/music_category_model.dart';
import '../repositories/music_repository.dart';

class GetMusicUseCase {
  final MusicRepository musicRepository;

  GetMusicUseCase({
    required this.musicRepository,
  });

  List<MusicCategory> getCategorizedMusic() {
    return musicRepository.getCategorizedMusic();
  }
}