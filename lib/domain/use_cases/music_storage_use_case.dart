import 'package:visma_tech_task_gabrielius_pileckis/data/enums/storage_options_enum.dart';

import '../../data/models/music_model.dart';
import '../repositories/music_storage_repository.dart';

class MusicStorageUseCase {
  final MusicStorageRepository musicStorageRepository;

  MusicStorageUseCase({
    required this.musicStorageRepository,
  });

  List<Music> getInitialMemoryMusicList() {
    final music = musicStorageRepository.getInitialMusicList();
    musicStorageRepository.saveInitialMusicToMemory(music);
    return music;
  }

  List<Music> getInitialFileSystemMusicList() {
    final music = musicStorageRepository.getInitialMusicList();
    musicStorageRepository.saveInitialMusicToFileSystemMemory(music);
    return musicStorageRepository.getFileSystemSavedMusic();
  }

  List<Music> getMemorySavedMusic() {
    return musicStorageRepository.getMemorySavedMusic();
  }

  List<Music> getFileSystemSavedMusic() {
    return musicStorageRepository.getFileSystemSavedMusic();
  }

  Music saveMusicToMemory(Music music) {
    return musicStorageRepository.saveMusicToMemory(
      music.copyWith(storageStatus: StorageStatus.memory),
    );
  }

  Future<Music> saveMusicToFileSystem(Music music) {
    return musicStorageRepository.saveMusicToFileSystem(
      music.copyWith(storageStatus: StorageStatus.fileSystem),
    );
  }
}
