extension SongsDurationExtention on Duration {
  String formatDurationToSongsLength() {
    final mm = (inMinutes % 60).toString().padLeft(2);
    final ss = (inSeconds % 60).toString().padLeft(2, '0');

    return '${mm}m ${ss}s';
  }
}