import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';

import 'di/app_injector.dart';
import 'domain/use_cases/get_music_use_case.dart';
import 'domain/use_cases/music_storage_use_case.dart';
import 'presentation/blocs/file_system_music_storage_cubit/file_system_music_storage_cubit.dart';
import 'presentation/blocs/get_music_cubit/get_music_cubit.dart';
import 'presentation/blocs/memory_music_storage_cubit/memory_music_storage_cubit.dart';
import 'presentation/blocs/theme_cubit/theme_cubit.dart';
import 'presentation/ui/pages/home_page.dart';
import 'presentation/ui/themes/app_theme.dart';

GetIt getIt = GetIt.instance;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await AppInjector.setupInjector();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: _createBlocs(),
      child: BlocBuilder<ThemeCubit, ThemeState>(
        builder: (context, state) {
          return MaterialApp(
            theme: AppThemes.lightTheme,
            darkTheme: AppThemes.darkTheme,
            themeMode: state.mode,
            home: const HomePage(),
          );
        },
      ),
    );
  }

  List<BlocProvider> _createBlocs() {
    return [
      BlocProvider<GetMusicCubit>(
        create: (_) => GetMusicCubit(
          getMusicUseCase: getIt.get<GetMusicUseCase>(),
        ),
      ),
      BlocProvider<MemoryMusicStorageCubit>(
        create: (_) => MemoryMusicStorageCubit(
          musicStorageUseCase: getIt.get<MusicStorageUseCase>(),
        )..initMusic(),
      ),
      BlocProvider<FileSystemMusicStorageCubit>(
        create: (_) => FileSystemMusicStorageCubit(
          musicStorageUseCase: getIt.get<MusicStorageUseCase>(),
        )..initMusic(),
      ),
      BlocProvider<ThemeCubit>(
        create: (_) => ThemeCubit(ThemeMode.light),
      ),
    ];
  }
}
